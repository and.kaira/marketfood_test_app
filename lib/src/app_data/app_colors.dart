import 'package:flutter/cupertino.dart';

class AppColors {
  AppColors._();

  static const Color white = Color(0xFFFFFFFF);
  static const Color black = Color(0xFF000000);
  static const Color transparent = Color(0x00000000);
  static const Color grey = Color(0xFFC2C2C5);
  static const Color appBarBackground = Color(0xFF1D1D27);
  static const Color primary = Color(0xFFFD823E);
  static const Color red = Color(0xFFD21404);
  static const Color selectIndicator = Color(0xFF5E8A4F);

}

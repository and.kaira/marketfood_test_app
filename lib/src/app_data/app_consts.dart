class AppConsts {
  AppConsts._();

  static const String appName = 'Test App';


  static const double kAppBarHeight = 70.0;
  static const double kBottomBarHeight = 80.0;
}

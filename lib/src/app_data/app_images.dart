class AppImages {
  AppImages._();

  static const String _path = 'assets/images/';

  static const String imageFoodPlaceholder = _path + 'food-placeholder.jpg';
}

import 'package:flutter/material.dart';

class AppFonts {
  AppFonts._();

  static TextStyle get roboto => const TextStyle(
    fontFamily: 'Roboto',
  );

  static TextStyle get robotoBold => const TextStyle(
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w700,
  );

  static TextStyle get robotoLight => const TextStyle(
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w400,
  );
}

import 'package:flutter/material.dart';

import '../app_data/app_images.dart';

Widget errorImageBuilder(BuildContext context, Object _, StackTrace? trace) {
  return Image.asset(
    AppImages.imageFoodPlaceholder,
    fit: BoxFit.cover,
  );
}

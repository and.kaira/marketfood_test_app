import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:marketfood_test_app/src/app_data/app_consts.dart';
import 'package:marketfood_test_app/src/application/app_theme/app_theme.dart';
import 'package:go_router/go_router.dart';

import 'app_router/app_router.dart';

class Application extends StatelessWidget {
  Application({Key? key}) : super(key: key);

  final GoRouter _router = AppRouter.goRouter;

  @override
  Widget build(BuildContext context) {

    return ProviderScope(
      child: MaterialApp.router(
        routerConfig: _router,
        debugShowCheckedModeBanner: false,
        theme: AppTheme.light,
        title: AppConsts.appName,
      ),
    );
  }
}

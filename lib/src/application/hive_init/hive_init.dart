import 'package:hive_flutter/hive_flutter.dart';

import 'hive_boxes.dart';

class HiveInit {
  HiveInit._();

  static Future<void> init() async {
    /// Init hive
    await Hive.initFlutter();

    /// Init boxes
    await Hive.openBox(HiveBoxes.example);
  }
}

import 'package:flutter/material.dart';
import 'package:marketfood_test_app/features/cart_page/presentation/pages/cart_page.dart';
import 'package:marketfood_test_app/features/home_page/data/models/story_response.dart';
import 'package:marketfood_test_app/features/home_page/presentation/pages/home_page.dart';
import 'package:marketfood_test_app/features/view_page/presentation/pages/view_page.dart';
import 'package:marketfood_test_app/src/application/app_router/app_scaffold.dart';
import 'package:go_router/go_router.dart';

class AppRouter {
  AppRouter._();

  static GoRouter get goRouter => GoRouter(
        routes: [
          ShellRoute(
            builder: (context, state, child) {
              return AppScaffold(
                location: state.subloc,
                child: child,
              );
            },
            routes: <RouteBase>[
              GoRoute(
                path: HomePage.route,
                builder: (BuildContext context, GoRouterState state) {
                  return const HomePage();
                },
              ),
              GoRoute(
                path: CartPage.route,
                builder: (BuildContext context, GoRouterState state) {
                  return const CartPage();
                },
              ),
              GoRoute(
                path: ViewPage.route,
                builder: (BuildContext context, GoRouterState state) {
                  return ViewPage(bannerImage: state.extra as BannerImage);
                },
              ),
            ],
          )
        ],
      );
}

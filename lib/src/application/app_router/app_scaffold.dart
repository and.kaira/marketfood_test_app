import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:marketfood_test_app/src/app_data/app_colors.dart';
import 'package:marketfood_test_app/src/widgets/main_app_bar/main_app_bar.dart';
import 'package:marketfood_test_app/src/widgets/main_bottom_bar/main_bottom_bar.dart';

import '../../../features/home_page/presentation/provider/home_page_provider.dart';

class AppScaffold extends HookWidget {
  final Widget child;
  final String location;

  const AppScaffold({
    Key? key,
    required this.child,
    required this.location,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    StateNotifierProvider<HomePageProvider, HomePageState>((ref) {
      return HomePageProvider();
    });
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: MainAppBar(location: location),
      bottomNavigationBar: MainBottomBar(location: location),
      body: child,
    );
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';

class FirebaseInit {
  FirebaseInit._();

  static late FirebaseFirestore fireStore;

  static Future<void> init() async {
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );
    fireStore = FirebaseFirestore.instance;
  }
}

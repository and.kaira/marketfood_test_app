import "package:dio/dio.dart";
import 'package:logger/logger.dart';

part 'logging.dart';

class DioClient {
  DioClient._();

  static const _baseUrl = "https://api.storyblok.com/v2/cdn/stories";

  static final Dio dio = Dio(
    BaseOptions(
      baseUrl: _baseUrl,
      connectTimeout: 5000,
      receiveTimeout: 3000,
    ),
  )..interceptors.add(Logging());
}

import 'package:flutter/material.dart';
import 'package:marketfood_test_app/src/app_data/app_colors.dart';

class AppTheme {
  AppTheme._();

  static ThemeData get light {
    final darkTheme = ThemeData.light();

    return darkTheme.copyWith(
      primaryColor: AppColors.primary,
      appBarTheme: const AppBarTheme(
        backgroundColor: AppColors.appBarBackground,
      ),
    );
  }
}

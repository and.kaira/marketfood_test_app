import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:marketfood_test_app/src/app_data/app_colors.dart';
import 'package:marketfood_test_app/src/app_data/app_fonts.dart';

class MainBottomBarItem extends HookWidget {
  final IconData icon;
  final VoidCallback onTap;
  final String title;
  final bool isEnable;
  final int? count;

  const MainBottomBarItem({
    required this.icon,
    required this.onTap,
    required this.title,
    this.count,
    this.isEnable = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Stack(
        alignment: Alignment.topRight,
        children: [
          Column(
            children: [
              Icon(
                icon,
                color: isEnable ? AppColors.primary : AppColors.grey,
                size: 36,
              ),
              const SizedBox(height: 4),
              Text(
                title,
                style: AppFonts.robotoBold.copyWith(
                  color: isEnable ? AppColors.primary : AppColors.grey,
                  fontSize: 14,
                ),
              ),
            ],
          ),
          if (count != null && count != 0)
            Container(
              height: 18.0,
              width: 18.0,
              decoration: BoxDecoration(
                color: AppColors.red,
                shape: BoxShape.circle,
              ),
              alignment: Alignment.center,
              child: Text(
                count!.toString(),
                style: AppFonts.roboto.copyWith(color: AppColors.white),
              ),
            ),
        ],
      ),
    );
  }
}

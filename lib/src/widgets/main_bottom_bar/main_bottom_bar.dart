import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:marketfood_test_app/features/cart_page/presentation/pages/cart_page.dart';
import 'package:marketfood_test_app/features/cart_page/presentation/provider/cart_page_provider.dart';
import 'package:marketfood_test_app/features/home_page/presentation/pages/home_page.dart';
import 'package:marketfood_test_app/src/app_data/app_colors.dart';
import 'package:marketfood_test_app/src/app_data/app_consts.dart';

import 'main_bottom_bar_item.dart';

class MainBottomBar extends HookConsumerWidget {
  final String location;

  const MainBottomBar({
    required this.location,
    Key? key,
  }) : super(key: key);

  static const String _homeTitle = 'Home';
  static const String _cartTitle = 'Cart';

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final stateCartPage = ref.watch(cartPageProvider);
    return Container(
      height: AppConsts.kBottomBarHeight,
      color: AppColors.white,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            MainBottomBarItem(
              onTap: () => GoRouter.of(context).go(HomePage.route),
              icon: Icons.home_outlined,
              title: _homeTitle,
              isEnable: location == HomePage.route,
            ),
            Consumer(
              builder: (context, ref, _) {
                return MainBottomBarItem(
                  onTap: () => GoRouter.of(context).go(CartPage.route),
                  icon: Icons.shopping_cart_outlined,
                  title: _cartTitle,
                  isEnable: location == CartPage.route,
                  count: stateCartPage.cart.cartDishes.length,
                );
              }
            ),
          ],
        ),
      ),
    );
  }
}

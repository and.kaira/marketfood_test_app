import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:marketfood_test_app/src/app_data/app_colors.dart';
import 'package:marketfood_test_app/src/app_data/app_consts.dart';
import 'package:marketfood_test_app/src/app_data/app_fonts.dart';

class MainAppBar extends HookWidget implements PreferredSizeWidget {
  final String location;

  const MainAppBar({
    Key? key,
    required this.location,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    FocusNode searchFieldFocus = useFocusNode();

    return AppBar(
      automaticallyImplyLeading: false,
      title: Center(
        child: Row(
          children: [
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.directions_car_filled_outlined),
            ),
            Text(
              AppConsts.appName,
              style: AppFonts.roboto.copyWith(
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
            ),
            const Spacer(),
            Container(
              alignment: Alignment.centerRight,
              width: 180.0,
              child: AnimatedBuilder(
                animation: searchFieldFocus,
                builder: (BuildContext context, Widget? child) {
                  return AnimatedSwitcher(
                    duration: const Duration(milliseconds: 500),
                    transitionBuilder: _defaultTransitionBuilder,
                    child: _getCurrentWidgetSearch(searchFieldFocus, context),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(AppConsts.kAppBarHeight);

  Widget _defaultTransitionBuilder(child, animation) {
    final offsetAnimation = Tween(
      begin: const Offset(1.0, 0.0),
      end: const Offset(0.0, 0.0),
    ).animate(animation);
    return ClipRect(
      child: SlideTransition(
        position: offsetAnimation,
        child: child,
      ),
    );
  }

  Widget _getCurrentWidgetSearch(FocusNode searchFieldFocus, BuildContext context) {
    if (searchFieldFocus.hasFocus) {
      return Container(
        width: 180.0,
        height: 50.0,
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Row(
            children: [
              Expanded(
                child: TextField(
                  focusNode: searchFieldFocus,
                  style: AppFonts.roboto.copyWith(color: AppColors.white),
                  cursorColor: AppColors.primary,
                  decoration: InputDecoration(
                    hintText: 'Search',
                    hintStyle: AppFonts.roboto.copyWith(color: AppColors.white),
                    filled: true,
                    fillColor: AppColors.white.withOpacity(0.2),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.primary),
                    ),
                  ),
                ),
              ),
              IconButton(
                onPressed: searchFieldFocus.unfocus,
                icon: Icon(Icons.close),
              )
            ],
          ),
        ),
      );
    } else {
      return IconButton(
        icon: const Icon(
          Icons.search,
          color: AppColors.primary,
          size: 30.0,
        ),
        onPressed: () {
          FocusScope.of(context).requestFocus(searchFieldFocus);
        },
      );
    }
  }
}

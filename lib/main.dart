import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:marketfood_test_app/src/app_data/app_colors.dart';
import 'package:marketfood_test_app/src/application/application.dart';
import 'package:marketfood_test_app/src/application/firebase_init/firebase_init.dart';
import 'package:marketfood_test_app/src/application/hive_init/hive_init.dart';

void main() async {
  await HiveInit.init();
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      systemNavigationBarColor: AppColors.white,
      statusBarColor: AppColors.appBarBackground,
      statusBarIconBrightness: Brightness.light,
      systemNavigationBarIconBrightness: Brightness.dark,
    ),
  );
  await FirebaseInit.init();

  runApp(Application());
}

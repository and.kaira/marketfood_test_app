import 'package:marketfood_test_app/features/home_page/data/models/story_response.dart';

abstract class IHomePageDataSource {
  Future<Story?> getBanners();
}

// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'story_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

StoryResponse _$StoryResponseFromJson(Map<String, dynamic> json) {
  return _StoryResponse.fromJson(json);
}

/// @nodoc
mixin _$StoryResponse {
  Story get story => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $StoryResponseCopyWith<StoryResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StoryResponseCopyWith<$Res> {
  factory $StoryResponseCopyWith(
          StoryResponse value, $Res Function(StoryResponse) then) =
      _$StoryResponseCopyWithImpl<$Res>;
  $Res call({Story story});

  $StoryCopyWith<$Res> get story;
}

/// @nodoc
class _$StoryResponseCopyWithImpl<$Res>
    implements $StoryResponseCopyWith<$Res> {
  _$StoryResponseCopyWithImpl(this._value, this._then);

  final StoryResponse _value;
  // ignore: unused_field
  final $Res Function(StoryResponse) _then;

  @override
  $Res call({
    Object? story = freezed,
  }) {
    return _then(_value.copyWith(
      story: story == freezed
          ? _value.story
          : story // ignore: cast_nullable_to_non_nullable
              as Story,
    ));
  }

  @override
  $StoryCopyWith<$Res> get story {
    return $StoryCopyWith<$Res>(_value.story, (value) {
      return _then(_value.copyWith(story: value));
    });
  }
}

/// @nodoc
abstract class _$$_StoryResponseCopyWith<$Res>
    implements $StoryResponseCopyWith<$Res> {
  factory _$$_StoryResponseCopyWith(
          _$_StoryResponse value, $Res Function(_$_StoryResponse) then) =
      __$$_StoryResponseCopyWithImpl<$Res>;
  @override
  $Res call({Story story});

  @override
  $StoryCopyWith<$Res> get story;
}

/// @nodoc
class __$$_StoryResponseCopyWithImpl<$Res>
    extends _$StoryResponseCopyWithImpl<$Res>
    implements _$$_StoryResponseCopyWith<$Res> {
  __$$_StoryResponseCopyWithImpl(
      _$_StoryResponse _value, $Res Function(_$_StoryResponse) _then)
      : super(_value, (v) => _then(v as _$_StoryResponse));

  @override
  _$_StoryResponse get _value => super._value as _$_StoryResponse;

  @override
  $Res call({
    Object? story = freezed,
  }) {
    return _then(_$_StoryResponse(
      story: story == freezed
          ? _value.story
          : story // ignore: cast_nullable_to_non_nullable
              as Story,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_StoryResponse implements _StoryResponse {
  const _$_StoryResponse({required this.story});

  factory _$_StoryResponse.fromJson(Map<String, dynamic> json) =>
      _$$_StoryResponseFromJson(json);

  @override
  final Story story;

  @override
  String toString() {
    return 'StoryResponse(story: $story)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_StoryResponse &&
            const DeepCollectionEquality().equals(other.story, story));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(story));

  @JsonKey(ignore: true)
  @override
  _$$_StoryResponseCopyWith<_$_StoryResponse> get copyWith =>
      __$$_StoryResponseCopyWithImpl<_$_StoryResponse>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_StoryResponseToJson(
      this,
    );
  }
}

abstract class _StoryResponse implements StoryResponse {
  const factory _StoryResponse({required final Story story}) = _$_StoryResponse;

  factory _StoryResponse.fromJson(Map<String, dynamic> json) =
      _$_StoryResponse.fromJson;

  @override
  Story get story;
  @override
  @JsonKey(ignore: true)
  _$$_StoryResponseCopyWith<_$_StoryResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

BannerImage _$BannerImageFromJson(Map<String, dynamic> json) {
  return _BannerImage.fromJson(json);
}

/// @nodoc
mixin _$BannerImage {
  @JsonKey(name: 'filename')
  String get image => throw _privateConstructorUsedError;
  int get id => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  String? get alt => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BannerImageCopyWith<BannerImage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BannerImageCopyWith<$Res> {
  factory $BannerImageCopyWith(
          BannerImage value, $Res Function(BannerImage) then) =
      _$BannerImageCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'filename') String image,
      int id,
      String title,
      String? alt});
}

/// @nodoc
class _$BannerImageCopyWithImpl<$Res> implements $BannerImageCopyWith<$Res> {
  _$BannerImageCopyWithImpl(this._value, this._then);

  final BannerImage _value;
  // ignore: unused_field
  final $Res Function(BannerImage) _then;

  @override
  $Res call({
    Object? image = freezed,
    Object? id = freezed,
    Object? title = freezed,
    Object? alt = freezed,
  }) {
    return _then(_value.copyWith(
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      alt: alt == freezed
          ? _value.alt
          : alt // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$$_BannerImageCopyWith<$Res>
    implements $BannerImageCopyWith<$Res> {
  factory _$$_BannerImageCopyWith(
          _$_BannerImage value, $Res Function(_$_BannerImage) then) =
      __$$_BannerImageCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'filename') String image,
      int id,
      String title,
      String? alt});
}

/// @nodoc
class __$$_BannerImageCopyWithImpl<$Res> extends _$BannerImageCopyWithImpl<$Res>
    implements _$$_BannerImageCopyWith<$Res> {
  __$$_BannerImageCopyWithImpl(
      _$_BannerImage _value, $Res Function(_$_BannerImage) _then)
      : super(_value, (v) => _then(v as _$_BannerImage));

  @override
  _$_BannerImage get _value => super._value as _$_BannerImage;

  @override
  $Res call({
    Object? image = freezed,
    Object? id = freezed,
    Object? title = freezed,
    Object? alt = freezed,
  }) {
    return _then(_$_BannerImage(
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      alt: alt == freezed
          ? _value.alt
          : alt // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BannerImage implements _BannerImage {
  const _$_BannerImage(
      {@JsonKey(name: 'filename') required this.image,
      required this.id,
      required this.title,
      required this.alt});

  factory _$_BannerImage.fromJson(Map<String, dynamic> json) =>
      _$$_BannerImageFromJson(json);

  @override
  @JsonKey(name: 'filename')
  final String image;
  @override
  final int id;
  @override
  final String title;
  @override
  final String? alt;

  @override
  String toString() {
    return 'BannerImage(image: $image, id: $id, title: $title, alt: $alt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BannerImage &&
            const DeepCollectionEquality().equals(other.image, image) &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality().equals(other.alt, alt));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(image),
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(title),
      const DeepCollectionEquality().hash(alt));

  @JsonKey(ignore: true)
  @override
  _$$_BannerImageCopyWith<_$_BannerImage> get copyWith =>
      __$$_BannerImageCopyWithImpl<_$_BannerImage>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BannerImageToJson(
      this,
    );
  }
}

abstract class _BannerImage implements BannerImage {
  const factory _BannerImage(
      {@JsonKey(name: 'filename') required final String image,
      required final int id,
      required final String title,
      required final String? alt}) = _$_BannerImage;

  factory _BannerImage.fromJson(Map<String, dynamic> json) =
      _$_BannerImage.fromJson;

  @override
  @JsonKey(name: 'filename')
  String get image;
  @override
  int get id;
  @override
  String get title;
  @override
  String? get alt;
  @override
  @JsonKey(ignore: true)
  _$$_BannerImageCopyWith<_$_BannerImage> get copyWith =>
      throw _privateConstructorUsedError;
}

Content _$ContentFromJson(Map<String, dynamic> json) {
  return _Content.fromJson(json);
}

/// @nodoc
mixin _$Content {
  @JsonKey(name: 'img')
  List<BannerImage> get images => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;
  String get body => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ContentCopyWith<Content> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ContentCopyWith<$Res> {
  factory $ContentCopyWith(Content value, $Res Function(Content) then) =
      _$ContentCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'img') List<BannerImage> images,
      String description,
      String body});
}

/// @nodoc
class _$ContentCopyWithImpl<$Res> implements $ContentCopyWith<$Res> {
  _$ContentCopyWithImpl(this._value, this._then);

  final Content _value;
  // ignore: unused_field
  final $Res Function(Content) _then;

  @override
  $Res call({
    Object? images = freezed,
    Object? description = freezed,
    Object? body = freezed,
  }) {
    return _then(_value.copyWith(
      images: images == freezed
          ? _value.images
          : images // ignore: cast_nullable_to_non_nullable
              as List<BannerImage>,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      body: body == freezed
          ? _value.body
          : body // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_ContentCopyWith<$Res> implements $ContentCopyWith<$Res> {
  factory _$$_ContentCopyWith(
          _$_Content value, $Res Function(_$_Content) then) =
      __$$_ContentCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'img') List<BannerImage> images,
      String description,
      String body});
}

/// @nodoc
class __$$_ContentCopyWithImpl<$Res> extends _$ContentCopyWithImpl<$Res>
    implements _$$_ContentCopyWith<$Res> {
  __$$_ContentCopyWithImpl(_$_Content _value, $Res Function(_$_Content) _then)
      : super(_value, (v) => _then(v as _$_Content));

  @override
  _$_Content get _value => super._value as _$_Content;

  @override
  $Res call({
    Object? images = freezed,
    Object? description = freezed,
    Object? body = freezed,
  }) {
    return _then(_$_Content(
      images: images == freezed
          ? _value.images
          : images // ignore: cast_nullable_to_non_nullable
              as List<BannerImage>,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      body: body == freezed
          ? _value.body
          : body // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Content implements _Content {
  const _$_Content(
      {@JsonKey(name: 'img') required this.images,
      required this.description,
      required this.body});

  factory _$_Content.fromJson(Map<String, dynamic> json) =>
      _$$_ContentFromJson(json);

  @override
  @JsonKey(name: 'img')
  final List<BannerImage> images;
  @override
  final String description;
  @override
  final String body;

  @override
  String toString() {
    return 'Content(images: $images, description: $description, body: $body)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Content &&
            const DeepCollectionEquality().equals(other.images, images) &&
            const DeepCollectionEquality()
                .equals(other.description, description) &&
            const DeepCollectionEquality().equals(other.body, body));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(images),
      const DeepCollectionEquality().hash(description),
      const DeepCollectionEquality().hash(body));

  @JsonKey(ignore: true)
  @override
  _$$_ContentCopyWith<_$_Content> get copyWith =>
      __$$_ContentCopyWithImpl<_$_Content>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ContentToJson(
      this,
    );
  }
}

abstract class _Content implements Content {
  const factory _Content(
      {@JsonKey(name: 'img') required final List<BannerImage> images,
      required final String description,
      required final String body}) = _$_Content;

  factory _Content.fromJson(Map<String, dynamic> json) = _$_Content.fromJson;

  @override
  @JsonKey(name: 'img')
  List<BannerImage> get images;
  @override
  String get description;
  @override
  String get body;
  @override
  @JsonKey(ignore: true)
  _$$_ContentCopyWith<_$_Content> get copyWith =>
      throw _privateConstructorUsedError;
}

Story _$StoryFromJson(Map<String, dynamic> json) {
  return _Story.fromJson(json);
}

/// @nodoc
mixin _$Story {
  Content get content => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $StoryCopyWith<Story> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StoryCopyWith<$Res> {
  factory $StoryCopyWith(Story value, $Res Function(Story) then) =
      _$StoryCopyWithImpl<$Res>;
  $Res call({Content content});

  $ContentCopyWith<$Res> get content;
}

/// @nodoc
class _$StoryCopyWithImpl<$Res> implements $StoryCopyWith<$Res> {
  _$StoryCopyWithImpl(this._value, this._then);

  final Story _value;
  // ignore: unused_field
  final $Res Function(Story) _then;

  @override
  $Res call({
    Object? content = freezed,
  }) {
    return _then(_value.copyWith(
      content: content == freezed
          ? _value.content
          : content // ignore: cast_nullable_to_non_nullable
              as Content,
    ));
  }

  @override
  $ContentCopyWith<$Res> get content {
    return $ContentCopyWith<$Res>(_value.content, (value) {
      return _then(_value.copyWith(content: value));
    });
  }
}

/// @nodoc
abstract class _$$_StoryCopyWith<$Res> implements $StoryCopyWith<$Res> {
  factory _$$_StoryCopyWith(_$_Story value, $Res Function(_$_Story) then) =
      __$$_StoryCopyWithImpl<$Res>;
  @override
  $Res call({Content content});

  @override
  $ContentCopyWith<$Res> get content;
}

/// @nodoc
class __$$_StoryCopyWithImpl<$Res> extends _$StoryCopyWithImpl<$Res>
    implements _$$_StoryCopyWith<$Res> {
  __$$_StoryCopyWithImpl(_$_Story _value, $Res Function(_$_Story) _then)
      : super(_value, (v) => _then(v as _$_Story));

  @override
  _$_Story get _value => super._value as _$_Story;

  @override
  $Res call({
    Object? content = freezed,
  }) {
    return _then(_$_Story(
      content: content == freezed
          ? _value.content
          : content // ignore: cast_nullable_to_non_nullable
              as Content,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Story implements _Story {
  const _$_Story({required this.content});

  factory _$_Story.fromJson(Map<String, dynamic> json) =>
      _$$_StoryFromJson(json);

  @override
  final Content content;

  @override
  String toString() {
    return 'Story(content: $content)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Story &&
            const DeepCollectionEquality().equals(other.content, content));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(content));

  @JsonKey(ignore: true)
  @override
  _$$_StoryCopyWith<_$_Story> get copyWith =>
      __$$_StoryCopyWithImpl<_$_Story>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_StoryToJson(
      this,
    );
  }
}

abstract class _Story implements Story {
  const factory _Story({required final Content content}) = _$_Story;

  factory _Story.fromJson(Map<String, dynamic> json) = _$_Story.fromJson;

  @override
  Content get content;
  @override
  @JsonKey(ignore: true)
  _$$_StoryCopyWith<_$_Story> get copyWith =>
      throw _privateConstructorUsedError;
}

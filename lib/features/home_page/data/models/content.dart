part of 'story_response.dart';

@Freezed(toJson: true, fromJson: true, makeCollectionsUnmodifiable: false)
class Content with _$Content {
  const factory Content({
    @JsonKey(name: 'img') required List<BannerImage> images,
    required String description,
    required String body,
  }) = _Content;

  factory Content.fromJson(Map<String, dynamic> json) =>
      _$ContentFromJson(json);
}

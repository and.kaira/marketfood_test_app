part of 'story_response.dart';

@Freezed(toJson: true, fromJson: true)
class BannerImage with _$BannerImage {
  const factory BannerImage({
    @JsonKey(name: 'filename') required String image,
    required int id,
    required String title,
    required String? alt,
  }) = _BannerImage;

  factory BannerImage.fromJson(Map<String, dynamic> json) => _$BannerImageFromJson(json);
}

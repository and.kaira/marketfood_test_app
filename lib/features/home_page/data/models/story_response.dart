import 'package:freezed_annotation/freezed_annotation.dart';

part 'banner_image.dart';
part 'content.dart';
part 'story.dart';

part 'story_response.freezed.dart';
part 'story_response.g.dart';

@Freezed(toJson: true, fromJson: true)
class StoryResponse with _$StoryResponse {
  const factory StoryResponse({
    required Story story,
  }) = _StoryResponse;

  factory StoryResponse.fromJson(Map<String, dynamic> json) =>
      _$StoryResponseFromJson(json);
}

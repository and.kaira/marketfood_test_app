part of 'story_response.dart';

@Freezed(toJson: true, fromJson: true)
class Story with _$Story {
  const factory Story({
    required Content content,
  }) = _Story;

  factory Story.fromJson(Map<String, dynamic> json) =>
      _$StoryFromJson(json);
}

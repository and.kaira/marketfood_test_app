// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'story_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_StoryResponse _$$_StoryResponseFromJson(Map<String, dynamic> json) =>
    _$_StoryResponse(
      story: Story.fromJson(json['story'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_StoryResponseToJson(_$_StoryResponse instance) =>
    <String, dynamic>{
      'story': instance.story,
    };

_$_BannerImage _$$_BannerImageFromJson(Map<String, dynamic> json) =>
    _$_BannerImage(
      image: json['filename'] as String,
      id: json['id'] as int,
      title: json['title'] as String,
      alt: json['alt'] as String?,
    );

Map<String, dynamic> _$$_BannerImageToJson(_$_BannerImage instance) =>
    <String, dynamic>{
      'filename': instance.image,
      'id': instance.id,
      'title': instance.title,
      'alt': instance.alt,
    };

_$_Content _$$_ContentFromJson(Map<String, dynamic> json) => _$_Content(
      images: (json['img'] as List<dynamic>)
          .map((e) => BannerImage.fromJson(e as Map<String, dynamic>))
          .toList(),
      description: json['description'] as String,
      body: json['body'] as String,
    );

Map<String, dynamic> _$$_ContentToJson(_$_Content instance) =>
    <String, dynamic>{
      'img': instance.images,
      'description': instance.description,
      'body': instance.body,
    };

_$_Story _$$_StoryFromJson(Map<String, dynamic> json) => _$_Story(
      content: Content.fromJson(json['content'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_StoryToJson(_$_Story instance) => <String, dynamic>{
      'content': instance.content,
    };

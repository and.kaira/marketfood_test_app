import 'package:marketfood_test_app/features/home_page/data/data_sources/i_home_page_data_sources.dart';
import 'package:marketfood_test_app/features/home_page/data/models/story_response.dart';

class HomePageRepository {
  final IHomePageDataSource _homePageDataSource;

  HomePageRepository(this._homePageDataSource);

  Future<Story?> getBanners() {
    return _homePageDataSource.getBanners();
  }
}

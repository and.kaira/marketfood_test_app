import 'package:dio/dio.dart';
import 'package:marketfood_test_app/features/home_page/data/data_sources/i_home_page_data_sources.dart';
import 'package:marketfood_test_app/features/home_page/data/models/story_response.dart';
import 'package:marketfood_test_app/src/application/api_client/dio_client.dart';

class HomePageUseCase implements IHomePageDataSource {
  @override
  Future<Story?> getBanners() async {
    final Response response = await DioClient.dio.get(
      '/marketfood',
      queryParameters: {
        'version': 'draft',
        'token': 'YX1dC80Z9U5IupBCCIbiRgtt',
        'cv': '1664543171',
      },
    );

    if (response.statusCode != 200) {
      throw response.statusMessage!;
    }
    final storyResponse = StoryResponse.fromJson(response.data);
    return storyResponse.story;
  }
}

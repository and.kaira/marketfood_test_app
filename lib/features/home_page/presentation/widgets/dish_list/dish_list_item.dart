import 'package:flutter/material.dart';
import 'package:marketfood_test_app/features/cart_page/data/models/dish/dish.dart';
import 'package:marketfood_test_app/src/app_data/app_colors.dart';
import 'package:marketfood_test_app/src/app_data/app_fonts.dart';
import 'package:marketfood_test_app/src/utils/error_image_util.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:transparent_image/transparent_image.dart';

import 'dish_modal_sheet_item.dart';

class DishListItem extends StatelessWidget {
  final Dish dish;
  final bool isSecond;

  const DishListItem({
    Key? key,
    required this.dish,
    this.isSecond = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(12.0),
      child: SizedBox(
        height: isSecond ? 150.0 : 200.0,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Positioned.fill(
              child: FadeInImage.memoryNetwork(
                placeholder: kTransparentImage,
                imageErrorBuilder: errorImageBuilder,
                fit: BoxFit.cover,
                image: dish.image,
              ),
            ),
            Material(
              color: AppColors.transparent,
              child: InkWell(
                onTap: () => _onTap(context),
              ),
            ),
            Container(
              width: double.maxFinite,
              color: AppColors.black.withOpacity(0.5),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  dish.name,
                  textAlign: TextAlign.center,
                  style: AppFonts.robotoBold.copyWith(
                    fontSize: 16,
                    color: AppColors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onTap(BuildContext context) {
    showMaterialModalBottomSheet(
      backgroundColor: AppColors.black.withOpacity(0.0),
      context: context,
      builder: (context) => DishModalSheetItem(dish: dish),
    );
  }
}

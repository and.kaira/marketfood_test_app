import 'package:flutter/material.dart';
import 'package:marketfood_test_app/features/cart_page/data/models/dish/dish.dart';
import 'package:marketfood_test_app/features/home_page/presentation/widgets/dish_list/dish_list_item.dart';
import 'package:waterfall_flow/waterfall_flow.dart';

class DishList extends StatelessWidget {
  final List<Dish> dishes;

  const DishList({
    Key? key,
    required this.dishes,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WaterfallFlow.builder(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      padding: EdgeInsets.all(5.0),
      itemCount: dishes.length,
      gridDelegate: SliverWaterfallFlowDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 3.0,
        mainAxisSpacing: 5.0,
        lastChildLayoutTypeBuilder: (index) =>
            index == dishes.length ? LastChildLayoutType.foot : LastChildLayoutType.none,
      ),
      itemBuilder: (BuildContext context, int index) {
        return DishListItem(
          dish: dishes[index],
          isSecond: index % 4 == 0,
        );
      },
    );
  }
}

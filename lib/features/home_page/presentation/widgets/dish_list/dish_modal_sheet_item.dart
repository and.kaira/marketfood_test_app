import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:marketfood_test_app/features/cart_page/data/models/cart_dish_item/cart_dish_item.dart';
import 'package:marketfood_test_app/features/cart_page/data/models/dish/dish.dart';
import 'package:marketfood_test_app/features/cart_page/presentation/provider/cart_page_provider.dart';
import 'package:marketfood_test_app/src/app_data/app_colors.dart';
import 'package:marketfood_test_app/src/app_data/app_fonts.dart';
import 'package:marketfood_test_app/src/utils/error_image_util.dart';
import 'package:marketfood_test_app/src/widgets/main_button/main_button.dart';
import 'package:transparent_image/transparent_image.dart';

class DishModalSheetItem extends HookConsumerWidget {
  final Dish dish;

  static const String _buttonText = 'Add to order';

  DishModalSheetItem({
    required this.dish,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final count = useState<int>(1);
    return ClipRRect(
      borderRadius: BorderRadius.vertical(
        top: Radius.circular(16.0),
      ),
      child: ColoredBox(
        color: AppColors.white,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              FadeInImage.memoryNetwork(
                height: 250.0,
                width: double.infinity,
                placeholder: kTransparentImage,
                imageErrorBuilder: errorImageBuilder,
                fit: BoxFit.cover,
                image: dish.image,
              ),
              const SizedBox(height: 16.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                child: Text(
                  dish.name,
                  style: AppFonts.robotoBold.copyWith(fontSize: 22),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                child: Text(
                  dish.description,
                  style: AppFonts.roboto.copyWith(fontSize: 18),
                ),
              ),
              const SizedBox(height: 16.0),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Count:',
                  style: AppFonts.roboto.copyWith(fontSize: 18),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  IconButton(
                    onPressed: () => count.value--,
                    icon: Icon(Icons.remove, color: Colors.red, size: 30),
                  ),
                  Text(
                    count.value.toString(),
                    style: AppFonts.robotoBold.copyWith(fontSize: 28),
                  ),
                  IconButton(
                    onPressed: () => count.value++,
                    icon: Icon(Icons.add, color: Colors.green, size: 30),
                  ),
                ],
              ),
              const SizedBox(height: 8.0),
              MainButton(
                onTap: () => onTap(context, ref, count.value),
                title: _buttonText,
              ),
              const SizedBox(height: 16.0),
            ],
          ),
        ),
      ),
    );
  }

  void onTap(BuildContext context, WidgetRef ref, int count) {
    final cartDish = CartDishItem(
      dish: dish,
      count: count,
    );
    ref.read<CartPageProvider>(cartPageProvider.notifier).addDishToCart(cartDish);
    Navigator.of(context).pop();
  }
}

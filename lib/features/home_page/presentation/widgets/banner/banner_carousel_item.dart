import 'package:flutter/material.dart';
import 'package:marketfood_test_app/features/home_page/data/models/story_response.dart';
import 'package:marketfood_test_app/features/view_page/presentation/pages/view_page.dart';
import 'package:marketfood_test_app/src/app_data/app_colors.dart';
import 'package:marketfood_test_app/src/app_data/app_fonts.dart';
import 'package:marketfood_test_app/src/utils/error_image_util.dart';
import 'package:marketfood_test_app/src/widgets/main_button/main_button.dart';
import 'package:go_router/go_router.dart';
import 'package:transparent_image/transparent_image.dart';

class BannerCarouselItem extends StatelessWidget {
  final BannerImage banner;
  final Content? content;

  const BannerCarouselItem({
    Key? key,
    required this.banner,
    required this.content,
  }) : super(key: key);

  static const String _buttonTitle = 'View Details';

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: [
        const ColoredBox(color: AppColors.black),
        FadeInImage.memoryNetwork(
          placeholder: kTransparentImage,
          imageErrorBuilder: errorImageBuilder,
          fit: BoxFit.cover,
          image: banner.image,
        ),
        DecoratedBox(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                AppColors.black.withOpacity(0.8),
                AppColors.transparent,
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                banner.title,
                style: AppFonts.robotoBold.copyWith(
                  fontSize: 28,
                  color: AppColors.white,
                ),
              ),
              const SizedBox(height: 16.0),
              if (content?.description != null)
                Text(
                  content!.description,
                  style: AppFonts.roboto.copyWith(
                    fontSize: 20,
                    color: AppColors.white,
                  ),
                ),
              const SizedBox(height: 16.0),
              MainButton(
                title: _buttonTitle,
                onTap: () => GoRouter.of(context).push(ViewPage.route, extra: banner),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

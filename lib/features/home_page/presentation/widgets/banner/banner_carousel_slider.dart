import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:marketfood_test_app/features/home_page/data/models/story_response.dart';
import 'package:marketfood_test_app/features/home_page/presentation/widgets/banner/banner_carousel_indicator.dart';
import 'package:marketfood_test_app/features/home_page/presentation/widgets/banner/banner_carousel_item.dart';

class BannerCarouselSlider extends HookWidget {
  final CarouselController carouselController = CarouselController();
  final Story story;

  BannerCarouselSlider({
    required this.story,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final currentPage = useState<int>(0);
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        CarouselSlider(
          carouselController: carouselController,
          items: story.content.images.map(
            (banner) {
              return BannerCarouselItem(
                banner: banner,
                content: story.content,
              );
            },
          ).toList(),
          options: CarouselOptions(
            onPageChanged: (page, _) => currentPage.value = page,
            aspectRatio: 16 / 9,
            height: 300,
            viewportFraction: 1,
            enableInfiniteScroll: true,
            reverse: false,
            autoPlay: true,
            autoPlayInterval: const Duration(seconds: 5),
            autoPlayAnimationDuration: const Duration(milliseconds: 800),
            autoPlayCurve: Curves.fastOutSlowIn,
          ),
        ),
        BannerCarouselIndicator(
          carouselController: carouselController,
          currentPage: currentPage.value,
          banners: story.content.images,
        ),
      ],
    );
  }
}

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:marketfood_test_app/features/home_page/data/models/story_response.dart';
import 'package:marketfood_test_app/features/home_page/presentation/widgets/banner/banner_carousel_indicator_item.dart';

class BannerCarouselIndicator extends StatelessWidget {
  final CarouselController carouselController;
  final int currentPage;
  final List<BannerImage> banners;

  const BannerCarouselIndicator({
    Key? key,
    required this.carouselController,
    required this.currentPage,
    required this.banners,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        for (int i = 0; i < banners.length; i++)
          BannerCarouselIndicatorItem(isEnable: currentPage == i),
      ],
    );
  }
}

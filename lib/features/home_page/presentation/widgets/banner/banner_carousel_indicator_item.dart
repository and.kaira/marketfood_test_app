import 'package:flutter/material.dart';
import 'package:marketfood_test_app/src/app_data/app_colors.dart';

class BannerCarouselIndicatorItem extends StatelessWidget {
  final bool isEnable;

  const BannerCarouselIndicatorItem({
    Key? key,
    required this.isEnable,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      width: 16.0,
      height: 16.0,
      margin: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 6.0),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: isEnable ? AppColors.selectIndicator : AppColors.white,
      ),
      duration: const Duration(milliseconds: 200),
    );
  }
}

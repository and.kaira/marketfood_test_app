import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:logger/logger.dart';
import 'package:marketfood_test_app/features/home_page/data/models/story_response.dart';
import 'package:marketfood_test_app/features/home_page/domain/repositories/home_page_repository.dart';
import 'package:marketfood_test_app/features/home_page/domain/use_cases/home_page_use_case.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'home_page_state.dart';

part 'home_page_provider.freezed.dart';

final homePageProvider = StateNotifierProvider<HomePageProvider, HomePageState>((ref) {
  return HomePageProvider();
});

class HomePageProvider extends StateNotifier<HomePageState> {
  HomePageProvider() : super(const _InitalState()) {
    load();
  }

  final HomePageUseCase useCase = HomePageUseCase();
  late final HomePageRepository repository = HomePageRepository(useCase);

  Future<void> load() async {
    state = const LoadingHomePageState();
    try {
      final story = await repository.getBanners();
      state = SuccessHomePageState(story: story!);
    } catch (e) {
      Logger().e(e);
      state = ErrorHomePageState(error: e.toString());
    }
  }
}

part of 'home_page_provider.dart';

@Freezed(makeCollectionsUnmodifiable: false)
class HomePageState with _$HomePageState {
  const factory HomePageState.initial({
    Story? story,
  }) = _InitalState;

  const factory HomePageState.loading({
    Story? story,
  }) = LoadingHomePageState;

  const factory HomePageState.success({
    required Story story,
  }) = SuccessHomePageState;

  const factory HomePageState.error({
    required String error,
    Story? story,
  }) = ErrorHomePageState;
}

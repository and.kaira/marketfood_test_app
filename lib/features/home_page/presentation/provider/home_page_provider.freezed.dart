// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'home_page_provider.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$HomePageState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Story? story) initial,
    required TResult Function(Story? story) loading,
    required TResult Function(Story story) success,
    required TResult Function(String error, Story? story) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Story? story)? initial,
    TResult Function(Story? story)? loading,
    TResult Function(Story story)? success,
    TResult Function(String error, Story? story)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Story? story)? initial,
    TResult Function(Story? story)? loading,
    TResult Function(Story story)? success,
    TResult Function(String error, Story? story)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitalState value) initial,
    required TResult Function(LoadingHomePageState value) loading,
    required TResult Function(SuccessHomePageState value) success,
    required TResult Function(ErrorHomePageState value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingHomePageState value)? loading,
    TResult Function(SuccessHomePageState value)? success,
    TResult Function(ErrorHomePageState value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingHomePageState value)? loading,
    TResult Function(SuccessHomePageState value)? success,
    TResult Function(ErrorHomePageState value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomePageStateCopyWith<$Res> {
  factory $HomePageStateCopyWith(
          HomePageState value, $Res Function(HomePageState) then) =
      _$HomePageStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$HomePageStateCopyWithImpl<$Res>
    implements $HomePageStateCopyWith<$Res> {
  _$HomePageStateCopyWithImpl(this._value, this._then);

  final HomePageState _value;
  // ignore: unused_field
  final $Res Function(HomePageState) _then;
}

/// @nodoc
abstract class _$$_InitalStateCopyWith<$Res> {
  factory _$$_InitalStateCopyWith(
          _$_InitalState value, $Res Function(_$_InitalState) then) =
      __$$_InitalStateCopyWithImpl<$Res>;
  $Res call({Story? story});

  $StoryCopyWith<$Res>? get story;
}

/// @nodoc
class __$$_InitalStateCopyWithImpl<$Res>
    extends _$HomePageStateCopyWithImpl<$Res>
    implements _$$_InitalStateCopyWith<$Res> {
  __$$_InitalStateCopyWithImpl(
      _$_InitalState _value, $Res Function(_$_InitalState) _then)
      : super(_value, (v) => _then(v as _$_InitalState));

  @override
  _$_InitalState get _value => super._value as _$_InitalState;

  @override
  $Res call({
    Object? story = freezed,
  }) {
    return _then(_$_InitalState(
      story: story == freezed
          ? _value.story
          : story // ignore: cast_nullable_to_non_nullable
              as Story?,
    ));
  }

  @override
  $StoryCopyWith<$Res>? get story {
    if (_value.story == null) {
      return null;
    }

    return $StoryCopyWith<$Res>(_value.story!, (value) {
      return _then(_value.copyWith(story: value));
    });
  }
}

/// @nodoc

class _$_InitalState implements _InitalState {
  const _$_InitalState({this.story});

  @override
  final Story? story;

  @override
  String toString() {
    return 'HomePageState.initial(story: $story)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_InitalState &&
            const DeepCollectionEquality().equals(other.story, story));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(story));

  @JsonKey(ignore: true)
  @override
  _$$_InitalStateCopyWith<_$_InitalState> get copyWith =>
      __$$_InitalStateCopyWithImpl<_$_InitalState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Story? story) initial,
    required TResult Function(Story? story) loading,
    required TResult Function(Story story) success,
    required TResult Function(String error, Story? story) error,
  }) {
    return initial(story);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Story? story)? initial,
    TResult Function(Story? story)? loading,
    TResult Function(Story story)? success,
    TResult Function(String error, Story? story)? error,
  }) {
    return initial?.call(story);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Story? story)? initial,
    TResult Function(Story? story)? loading,
    TResult Function(Story story)? success,
    TResult Function(String error, Story? story)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(story);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitalState value) initial,
    required TResult Function(LoadingHomePageState value) loading,
    required TResult Function(SuccessHomePageState value) success,
    required TResult Function(ErrorHomePageState value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingHomePageState value)? loading,
    TResult Function(SuccessHomePageState value)? success,
    TResult Function(ErrorHomePageState value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingHomePageState value)? loading,
    TResult Function(SuccessHomePageState value)? success,
    TResult Function(ErrorHomePageState value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _InitalState implements HomePageState {
  const factory _InitalState({final Story? story}) = _$_InitalState;

  Story? get story;
  @JsonKey(ignore: true)
  _$$_InitalStateCopyWith<_$_InitalState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$LoadingHomePageStateCopyWith<$Res> {
  factory _$$LoadingHomePageStateCopyWith(_$LoadingHomePageState value,
          $Res Function(_$LoadingHomePageState) then) =
      __$$LoadingHomePageStateCopyWithImpl<$Res>;
  $Res call({Story? story});

  $StoryCopyWith<$Res>? get story;
}

/// @nodoc
class __$$LoadingHomePageStateCopyWithImpl<$Res>
    extends _$HomePageStateCopyWithImpl<$Res>
    implements _$$LoadingHomePageStateCopyWith<$Res> {
  __$$LoadingHomePageStateCopyWithImpl(_$LoadingHomePageState _value,
      $Res Function(_$LoadingHomePageState) _then)
      : super(_value, (v) => _then(v as _$LoadingHomePageState));

  @override
  _$LoadingHomePageState get _value => super._value as _$LoadingHomePageState;

  @override
  $Res call({
    Object? story = freezed,
  }) {
    return _then(_$LoadingHomePageState(
      story: story == freezed
          ? _value.story
          : story // ignore: cast_nullable_to_non_nullable
              as Story?,
    ));
  }

  @override
  $StoryCopyWith<$Res>? get story {
    if (_value.story == null) {
      return null;
    }

    return $StoryCopyWith<$Res>(_value.story!, (value) {
      return _then(_value.copyWith(story: value));
    });
  }
}

/// @nodoc

class _$LoadingHomePageState implements LoadingHomePageState {
  const _$LoadingHomePageState({this.story});

  @override
  final Story? story;

  @override
  String toString() {
    return 'HomePageState.loading(story: $story)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadingHomePageState &&
            const DeepCollectionEquality().equals(other.story, story));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(story));

  @JsonKey(ignore: true)
  @override
  _$$LoadingHomePageStateCopyWith<_$LoadingHomePageState> get copyWith =>
      __$$LoadingHomePageStateCopyWithImpl<_$LoadingHomePageState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Story? story) initial,
    required TResult Function(Story? story) loading,
    required TResult Function(Story story) success,
    required TResult Function(String error, Story? story) error,
  }) {
    return loading(story);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Story? story)? initial,
    TResult Function(Story? story)? loading,
    TResult Function(Story story)? success,
    TResult Function(String error, Story? story)? error,
  }) {
    return loading?.call(story);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Story? story)? initial,
    TResult Function(Story? story)? loading,
    TResult Function(Story story)? success,
    TResult Function(String error, Story? story)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(story);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitalState value) initial,
    required TResult Function(LoadingHomePageState value) loading,
    required TResult Function(SuccessHomePageState value) success,
    required TResult Function(ErrorHomePageState value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingHomePageState value)? loading,
    TResult Function(SuccessHomePageState value)? success,
    TResult Function(ErrorHomePageState value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingHomePageState value)? loading,
    TResult Function(SuccessHomePageState value)? success,
    TResult Function(ErrorHomePageState value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class LoadingHomePageState implements HomePageState {
  const factory LoadingHomePageState({final Story? story}) =
      _$LoadingHomePageState;

  Story? get story;
  @JsonKey(ignore: true)
  _$$LoadingHomePageStateCopyWith<_$LoadingHomePageState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SuccessHomePageStateCopyWith<$Res> {
  factory _$$SuccessHomePageStateCopyWith(_$SuccessHomePageState value,
          $Res Function(_$SuccessHomePageState) then) =
      __$$SuccessHomePageStateCopyWithImpl<$Res>;
  $Res call({Story story});

  $StoryCopyWith<$Res> get story;
}

/// @nodoc
class __$$SuccessHomePageStateCopyWithImpl<$Res>
    extends _$HomePageStateCopyWithImpl<$Res>
    implements _$$SuccessHomePageStateCopyWith<$Res> {
  __$$SuccessHomePageStateCopyWithImpl(_$SuccessHomePageState _value,
      $Res Function(_$SuccessHomePageState) _then)
      : super(_value, (v) => _then(v as _$SuccessHomePageState));

  @override
  _$SuccessHomePageState get _value => super._value as _$SuccessHomePageState;

  @override
  $Res call({
    Object? story = freezed,
  }) {
    return _then(_$SuccessHomePageState(
      story: story == freezed
          ? _value.story
          : story // ignore: cast_nullable_to_non_nullable
              as Story,
    ));
  }

  @override
  $StoryCopyWith<$Res> get story {
    return $StoryCopyWith<$Res>(_value.story, (value) {
      return _then(_value.copyWith(story: value));
    });
  }
}

/// @nodoc

class _$SuccessHomePageState implements SuccessHomePageState {
  const _$SuccessHomePageState({required this.story});

  @override
  final Story story;

  @override
  String toString() {
    return 'HomePageState.success(story: $story)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SuccessHomePageState &&
            const DeepCollectionEquality().equals(other.story, story));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(story));

  @JsonKey(ignore: true)
  @override
  _$$SuccessHomePageStateCopyWith<_$SuccessHomePageState> get copyWith =>
      __$$SuccessHomePageStateCopyWithImpl<_$SuccessHomePageState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Story? story) initial,
    required TResult Function(Story? story) loading,
    required TResult Function(Story story) success,
    required TResult Function(String error, Story? story) error,
  }) {
    return success(story);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Story? story)? initial,
    TResult Function(Story? story)? loading,
    TResult Function(Story story)? success,
    TResult Function(String error, Story? story)? error,
  }) {
    return success?.call(story);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Story? story)? initial,
    TResult Function(Story? story)? loading,
    TResult Function(Story story)? success,
    TResult Function(String error, Story? story)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(story);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitalState value) initial,
    required TResult Function(LoadingHomePageState value) loading,
    required TResult Function(SuccessHomePageState value) success,
    required TResult Function(ErrorHomePageState value) error,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingHomePageState value)? loading,
    TResult Function(SuccessHomePageState value)? success,
    TResult Function(ErrorHomePageState value)? error,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingHomePageState value)? loading,
    TResult Function(SuccessHomePageState value)? success,
    TResult Function(ErrorHomePageState value)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class SuccessHomePageState implements HomePageState {
  const factory SuccessHomePageState({required final Story story}) =
      _$SuccessHomePageState;

  Story get story;
  @JsonKey(ignore: true)
  _$$SuccessHomePageStateCopyWith<_$SuccessHomePageState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ErrorHomePageStateCopyWith<$Res> {
  factory _$$ErrorHomePageStateCopyWith(_$ErrorHomePageState value,
          $Res Function(_$ErrorHomePageState) then) =
      __$$ErrorHomePageStateCopyWithImpl<$Res>;
  $Res call({String error, Story? story});

  $StoryCopyWith<$Res>? get story;
}

/// @nodoc
class __$$ErrorHomePageStateCopyWithImpl<$Res>
    extends _$HomePageStateCopyWithImpl<$Res>
    implements _$$ErrorHomePageStateCopyWith<$Res> {
  __$$ErrorHomePageStateCopyWithImpl(
      _$ErrorHomePageState _value, $Res Function(_$ErrorHomePageState) _then)
      : super(_value, (v) => _then(v as _$ErrorHomePageState));

  @override
  _$ErrorHomePageState get _value => super._value as _$ErrorHomePageState;

  @override
  $Res call({
    Object? error = freezed,
    Object? story = freezed,
  }) {
    return _then(_$ErrorHomePageState(
      error: error == freezed
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
      story: story == freezed
          ? _value.story
          : story // ignore: cast_nullable_to_non_nullable
              as Story?,
    ));
  }

  @override
  $StoryCopyWith<$Res>? get story {
    if (_value.story == null) {
      return null;
    }

    return $StoryCopyWith<$Res>(_value.story!, (value) {
      return _then(_value.copyWith(story: value));
    });
  }
}

/// @nodoc

class _$ErrorHomePageState implements ErrorHomePageState {
  const _$ErrorHomePageState({required this.error, this.story});

  @override
  final String error;
  @override
  final Story? story;

  @override
  String toString() {
    return 'HomePageState.error(error: $error, story: $story)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ErrorHomePageState &&
            const DeepCollectionEquality().equals(other.error, error) &&
            const DeepCollectionEquality().equals(other.story, story));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(error),
      const DeepCollectionEquality().hash(story));

  @JsonKey(ignore: true)
  @override
  _$$ErrorHomePageStateCopyWith<_$ErrorHomePageState> get copyWith =>
      __$$ErrorHomePageStateCopyWithImpl<_$ErrorHomePageState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Story? story) initial,
    required TResult Function(Story? story) loading,
    required TResult Function(Story story) success,
    required TResult Function(String error, Story? story) error,
  }) {
    return error(this.error, story);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Story? story)? initial,
    TResult Function(Story? story)? loading,
    TResult Function(Story story)? success,
    TResult Function(String error, Story? story)? error,
  }) {
    return error?.call(this.error, story);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Story? story)? initial,
    TResult Function(Story? story)? loading,
    TResult Function(Story story)? success,
    TResult Function(String error, Story? story)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this.error, story);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitalState value) initial,
    required TResult Function(LoadingHomePageState value) loading,
    required TResult Function(SuccessHomePageState value) success,
    required TResult Function(ErrorHomePageState value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingHomePageState value)? loading,
    TResult Function(SuccessHomePageState value)? success,
    TResult Function(ErrorHomePageState value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingHomePageState value)? loading,
    TResult Function(SuccessHomePageState value)? success,
    TResult Function(ErrorHomePageState value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class ErrorHomePageState implements HomePageState {
  const factory ErrorHomePageState(
      {required final String error, final Story? story}) = _$ErrorHomePageState;

  String get error;
  Story? get story;
  @JsonKey(ignore: true)
  _$$ErrorHomePageStateCopyWith<_$ErrorHomePageState> get copyWith =>
      throw _privateConstructorUsedError;
}

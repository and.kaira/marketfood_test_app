import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:marketfood_test_app/features/cart_page/presentation/provider/cart_page_provider.dart';
import 'package:marketfood_test_app/features/home_page/presentation/provider/home_page_provider.dart';
import 'package:marketfood_test_app/features/home_page/presentation/widgets/banner/banner_carousel_slider.dart';
import 'package:marketfood_test_app/features/home_page/presentation/widgets/dish_list/dish_list.dart';
import 'package:marketfood_test_app/src/app_data/app_colors.dart';
import 'package:marketfood_test_app/src/app_data/app_fonts.dart';

class HomePage extends HookConsumerWidget {
  const HomePage({Key? key}) : super(key: key);

  static const String route = '/';

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final stateHomePage = ref.watch(homePageProvider);
    final stateCartPage = ref.watch(cartPageProvider);

    if (stateHomePage is SuccessHomePageState && stateCartPage is SuccessCartPageState) {
      return ColoredBox(
        color: AppColors.white,
        child: SingleChildScrollView(
          child: Column(
            children: [
              BannerCarouselSlider(story: stateHomePage.story),
              DishList(dishes: stateCartPage.dishes!),
            ],
          ),
        ),
      );
    } else if (stateHomePage is ErrorHomePageState || stateCartPage is ErrorCartPageState) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(Icons.error, size: 50, color: Colors.red),
          SizedBox(height: 24.0),
          Text(
            stateHomePage is ErrorHomePageState
                ? stateHomePage.error
                : (stateCartPage as ErrorCartPageState).error,
            textAlign: TextAlign.center,
            style: AppFonts.robotoBold.copyWith(fontSize: 20),
          ),
        ],
      );
    } else {
      return const Center(
        child: CircularProgressIndicator(
          color: AppColors.primary,
        ),
      );
    }
  }
}

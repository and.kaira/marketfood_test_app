import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:marketfood_test_app/features/home_page/data/models/story_response.dart';
import 'package:marketfood_test_app/features/home_page/presentation/pages/home_page.dart';
import 'package:marketfood_test_app/src/app_data/app_colors.dart';
import 'package:marketfood_test_app/src/app_data/app_fonts.dart';
import 'package:marketfood_test_app/src/utils/error_image_util.dart';
import 'package:marketfood_test_app/src/widgets/main_button/main_button.dart';
import 'package:transparent_image/transparent_image.dart';

class ViewPage extends StatelessWidget {
  final BannerImage bannerImage;

  const ViewPage({Key? key, required this.bannerImage}) : super(key: key);

  static const String route = '/view';

  static const String _buttonText = 'Hurry up and order!';

  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: AppColors.white,
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            FadeInImage.memoryNetwork(
              width: double.infinity,
              placeholder: kTransparentImage,
              imageErrorBuilder: errorImageBuilder,
              fit: BoxFit.fitHeight,
              image: bannerImage.image,
            ),
            const SizedBox(height: 16.0),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: Text(
                bannerImage.title,
                style: AppFonts.robotoBold.copyWith(fontSize: 22),
              ),
            ),
            if (bannerImage.alt != null)
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                child: Text(
                  bannerImage.alt!,
                  style: AppFonts.roboto.copyWith(fontSize: 18),
                ),
              ),
            const SizedBox(height: 8.0),
            MainButton(
              onTap: () => GoRouter.of(context).go(HomePage.route),
              title: _buttonText,
            ),
            const SizedBox(height: 16.0),
          ],
        ),
      ),
    );
  }
}

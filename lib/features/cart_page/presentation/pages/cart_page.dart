import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:marketfood_test_app/features/cart_page/presentation/provider/cart_page_provider.dart';
import 'package:marketfood_test_app/features/cart_page/presentation/widgets/cart_list.dart';
import 'package:marketfood_test_app/features/home_page/presentation/pages/home_page.dart';
import 'package:marketfood_test_app/src/app_data/app_colors.dart';
import 'package:marketfood_test_app/src/app_data/app_fonts.dart';
import 'package:marketfood_test_app/src/widgets/main_button/main_button.dart';

class CartPage extends HookConsumerWidget {
  const CartPage({Key? key}) : super(key: key);

  static const String route = '/cart';

  static const String _buttonHomePageText = 'Order a delicious meal!';
  static const String _noCartItemText = 'You haven\'t added anything yet! 😢';
  static const String _buttonPurchaseText = 'Purchase';

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final stateCartPage = ref.watch(cartPageProvider);
    final provider = ref.read<CartPageProvider>(cartPageProvider.notifier);
    if (stateCartPage.cart.cartDishes.isEmpty) {
      return ColoredBox(
        color: AppColors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(_noCartItemText, style: AppFonts.roboto.copyWith(fontSize: 20)),
            const SizedBox(height: 16.0),
            MainButton(
              onTap: () => GoRouter.of(context).go(HomePage.route),
              title: _buttonHomePageText,
            )
          ],
        ),
      );
    } else {
      return ColoredBox(
        color: AppColors.white,
        child: Column(
          children: [
            Expanded(
              child: CartList(cart: stateCartPage.cart),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: MainButton(onTap: provider.purchase, title: _buttonPurchaseText),
            ),
          ],
        ),
      );
    }
  }
}

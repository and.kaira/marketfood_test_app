import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:logger/logger.dart';
import 'package:marketfood_test_app/features/cart_page/data/models/cart/cart.dart';
import 'package:marketfood_test_app/features/cart_page/data/models/cart_dish_item/cart_dish_item.dart';
import 'package:marketfood_test_app/features/cart_page/data/models/dish/dish.dart';
import 'package:marketfood_test_app/features/cart_page/domain/repositories/cart_page_repository.dart';
import 'package:marketfood_test_app/features/cart_page/domain/use_cases/cart_page_use_cases.dart';

part 'cart_page_provider.freezed.dart';

part 'cart_page_state.dart';

final cartPageProvider = StateNotifierProvider<CartPageProvider, CartPageState>((ref) {
  return CartPageProvider();
});

class CartPageProvider extends StateNotifier<CartPageState> {
  CartPageProvider() : super(const _InitalState(cart: Cart(cartDishes: []))) {
    load();
  }

  final CartPageUseCase useCase = CartPageUseCase();
  late final CartPageRepository repository = CartPageRepository(useCase);

  Future<void> load() async {
    state = const CartPageState.loading(cart: Cart(cartDishes: []));
    try {
      final dishes = await repository.getDishes();
      state = SuccessCartPageState(dishes: dishes!, cart: Cart(cartDishes: []));
    } catch (e) {
      Logger().e(e);
      state = ErrorCartPageState(error: e.toString(), cart: Cart(cartDishes: []));
    }
  }

  void addDishToCart(CartDishItem cartDishItem) {
    final isMatch = !state.cart.cartDishes.any((element) => element.dish == cartDishItem.dish);
    if (state.cart.cartDishes.isEmpty || isMatch) {
      state = state.copyWith(cart: state.cart..cartDishes.add(cartDishItem));
    } else {
      List<CartDishItem> dishes = state.cart.cartDishes;
      dishes.firstWhere((element) => element.dish == cartDishItem.dish).count += cartDishItem.count;
      state = state.copyWith(cart: state.cart.copyWith(cartDishes: dishes));
    }
  }

  void removeDishToCart(CartDishItem cartDishItem) {
    state = state.copyWith(cart: state.cart..cartDishes.remove(cartDishItem));
  }

  void incrementCountCartDishItem(CartDishItem cartDishItem) {
    List<CartDishItem> dishes = state.cart.cartDishes;
    dishes.firstWhere((element) => element.dish == cartDishItem.dish).count++;
    state = state.copyWith(cart: state.cart.copyWith(cartDishes: dishes));
  }

  void decrementCountCartDishItem(CartDishItem cartDishItem) {
    List<CartDishItem> dishes = state.cart.cartDishes;
    dishes.firstWhere((element) => element.dish == cartDishItem.dish).count--;
    dishes.removeWhere((element) => element.count <= 0);
    state = state.copyWith(cart: state.cart.copyWith(cartDishes: dishes));
  }

  Future<void> purchase() async {}
}

// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'cart_page_provider.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CartPageState {
  Cart get cart => throw _privateConstructorUsedError;
  List<Dish>? get dishes => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Cart cart, List<Dish>? dishes) initial,
    required TResult Function(Cart cart, List<Dish>? dishes) loading,
    required TResult Function(Cart cart, List<Dish>? dishes) success,
    required TResult Function(String error, Cart cart, List<Dish>? dishes)
        error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Cart cart, List<Dish>? dishes)? initial,
    TResult Function(Cart cart, List<Dish>? dishes)? loading,
    TResult Function(Cart cart, List<Dish>? dishes)? success,
    TResult Function(String error, Cart cart, List<Dish>? dishes)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Cart cart, List<Dish>? dishes)? initial,
    TResult Function(Cart cart, List<Dish>? dishes)? loading,
    TResult Function(Cart cart, List<Dish>? dishes)? success,
    TResult Function(String error, Cart cart, List<Dish>? dishes)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitalState value) initial,
    required TResult Function(LoadingCartPageState value) loading,
    required TResult Function(SuccessCartPageState value) success,
    required TResult Function(ErrorCartPageState value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingCartPageState value)? loading,
    TResult Function(SuccessCartPageState value)? success,
    TResult Function(ErrorCartPageState value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingCartPageState value)? loading,
    TResult Function(SuccessCartPageState value)? success,
    TResult Function(ErrorCartPageState value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CartPageStateCopyWith<CartPageState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CartPageStateCopyWith<$Res> {
  factory $CartPageStateCopyWith(
          CartPageState value, $Res Function(CartPageState) then) =
      _$CartPageStateCopyWithImpl<$Res>;
  $Res call({Cart cart, List<Dish>? dishes});

  $CartCopyWith<$Res> get cart;
}

/// @nodoc
class _$CartPageStateCopyWithImpl<$Res>
    implements $CartPageStateCopyWith<$Res> {
  _$CartPageStateCopyWithImpl(this._value, this._then);

  final CartPageState _value;
  // ignore: unused_field
  final $Res Function(CartPageState) _then;

  @override
  $Res call({
    Object? cart = freezed,
    Object? dishes = freezed,
  }) {
    return _then(_value.copyWith(
      cart: cart == freezed
          ? _value.cart
          : cart // ignore: cast_nullable_to_non_nullable
              as Cart,
      dishes: dishes == freezed
          ? _value.dishes
          : dishes // ignore: cast_nullable_to_non_nullable
              as List<Dish>?,
    ));
  }

  @override
  $CartCopyWith<$Res> get cart {
    return $CartCopyWith<$Res>(_value.cart, (value) {
      return _then(_value.copyWith(cart: value));
    });
  }
}

/// @nodoc
abstract class _$$_InitalStateCopyWith<$Res>
    implements $CartPageStateCopyWith<$Res> {
  factory _$$_InitalStateCopyWith(
          _$_InitalState value, $Res Function(_$_InitalState) then) =
      __$$_InitalStateCopyWithImpl<$Res>;
  @override
  $Res call({Cart cart, List<Dish>? dishes});

  @override
  $CartCopyWith<$Res> get cart;
}

/// @nodoc
class __$$_InitalStateCopyWithImpl<$Res>
    extends _$CartPageStateCopyWithImpl<$Res>
    implements _$$_InitalStateCopyWith<$Res> {
  __$$_InitalStateCopyWithImpl(
      _$_InitalState _value, $Res Function(_$_InitalState) _then)
      : super(_value, (v) => _then(v as _$_InitalState));

  @override
  _$_InitalState get _value => super._value as _$_InitalState;

  @override
  $Res call({
    Object? cart = freezed,
    Object? dishes = freezed,
  }) {
    return _then(_$_InitalState(
      cart: cart == freezed
          ? _value.cart
          : cart // ignore: cast_nullable_to_non_nullable
              as Cart,
      dishes: dishes == freezed
          ? _value.dishes
          : dishes // ignore: cast_nullable_to_non_nullable
              as List<Dish>?,
    ));
  }
}

/// @nodoc

class _$_InitalState implements _InitalState {
  const _$_InitalState({required this.cart, this.dishes});

  @override
  final Cart cart;
  @override
  final List<Dish>? dishes;

  @override
  String toString() {
    return 'CartPageState.initial(cart: $cart, dishes: $dishes)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_InitalState &&
            const DeepCollectionEquality().equals(other.cart, cart) &&
            const DeepCollectionEquality().equals(other.dishes, dishes));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(cart),
      const DeepCollectionEquality().hash(dishes));

  @JsonKey(ignore: true)
  @override
  _$$_InitalStateCopyWith<_$_InitalState> get copyWith =>
      __$$_InitalStateCopyWithImpl<_$_InitalState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Cart cart, List<Dish>? dishes) initial,
    required TResult Function(Cart cart, List<Dish>? dishes) loading,
    required TResult Function(Cart cart, List<Dish>? dishes) success,
    required TResult Function(String error, Cart cart, List<Dish>? dishes)
        error,
  }) {
    return initial(cart, dishes);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Cart cart, List<Dish>? dishes)? initial,
    TResult Function(Cart cart, List<Dish>? dishes)? loading,
    TResult Function(Cart cart, List<Dish>? dishes)? success,
    TResult Function(String error, Cart cart, List<Dish>? dishes)? error,
  }) {
    return initial?.call(cart, dishes);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Cart cart, List<Dish>? dishes)? initial,
    TResult Function(Cart cart, List<Dish>? dishes)? loading,
    TResult Function(Cart cart, List<Dish>? dishes)? success,
    TResult Function(String error, Cart cart, List<Dish>? dishes)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(cart, dishes);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitalState value) initial,
    required TResult Function(LoadingCartPageState value) loading,
    required TResult Function(SuccessCartPageState value) success,
    required TResult Function(ErrorCartPageState value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingCartPageState value)? loading,
    TResult Function(SuccessCartPageState value)? success,
    TResult Function(ErrorCartPageState value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingCartPageState value)? loading,
    TResult Function(SuccessCartPageState value)? success,
    TResult Function(ErrorCartPageState value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _InitalState implements CartPageState {
  const factory _InitalState(
      {required final Cart cart, final List<Dish>? dishes}) = _$_InitalState;

  @override
  Cart get cart;
  @override
  List<Dish>? get dishes;
  @override
  @JsonKey(ignore: true)
  _$$_InitalStateCopyWith<_$_InitalState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$LoadingCartPageStateCopyWith<$Res>
    implements $CartPageStateCopyWith<$Res> {
  factory _$$LoadingCartPageStateCopyWith(_$LoadingCartPageState value,
          $Res Function(_$LoadingCartPageState) then) =
      __$$LoadingCartPageStateCopyWithImpl<$Res>;
  @override
  $Res call({Cart cart, List<Dish>? dishes});

  @override
  $CartCopyWith<$Res> get cart;
}

/// @nodoc
class __$$LoadingCartPageStateCopyWithImpl<$Res>
    extends _$CartPageStateCopyWithImpl<$Res>
    implements _$$LoadingCartPageStateCopyWith<$Res> {
  __$$LoadingCartPageStateCopyWithImpl(_$LoadingCartPageState _value,
      $Res Function(_$LoadingCartPageState) _then)
      : super(_value, (v) => _then(v as _$LoadingCartPageState));

  @override
  _$LoadingCartPageState get _value => super._value as _$LoadingCartPageState;

  @override
  $Res call({
    Object? cart = freezed,
    Object? dishes = freezed,
  }) {
    return _then(_$LoadingCartPageState(
      cart: cart == freezed
          ? _value.cart
          : cart // ignore: cast_nullable_to_non_nullable
              as Cart,
      dishes: dishes == freezed
          ? _value.dishes
          : dishes // ignore: cast_nullable_to_non_nullable
              as List<Dish>?,
    ));
  }
}

/// @nodoc

class _$LoadingCartPageState implements LoadingCartPageState {
  const _$LoadingCartPageState({required this.cart, this.dishes});

  @override
  final Cart cart;
  @override
  final List<Dish>? dishes;

  @override
  String toString() {
    return 'CartPageState.loading(cart: $cart, dishes: $dishes)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadingCartPageState &&
            const DeepCollectionEquality().equals(other.cart, cart) &&
            const DeepCollectionEquality().equals(other.dishes, dishes));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(cart),
      const DeepCollectionEquality().hash(dishes));

  @JsonKey(ignore: true)
  @override
  _$$LoadingCartPageStateCopyWith<_$LoadingCartPageState> get copyWith =>
      __$$LoadingCartPageStateCopyWithImpl<_$LoadingCartPageState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Cart cart, List<Dish>? dishes) initial,
    required TResult Function(Cart cart, List<Dish>? dishes) loading,
    required TResult Function(Cart cart, List<Dish>? dishes) success,
    required TResult Function(String error, Cart cart, List<Dish>? dishes)
        error,
  }) {
    return loading(cart, dishes);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Cart cart, List<Dish>? dishes)? initial,
    TResult Function(Cart cart, List<Dish>? dishes)? loading,
    TResult Function(Cart cart, List<Dish>? dishes)? success,
    TResult Function(String error, Cart cart, List<Dish>? dishes)? error,
  }) {
    return loading?.call(cart, dishes);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Cart cart, List<Dish>? dishes)? initial,
    TResult Function(Cart cart, List<Dish>? dishes)? loading,
    TResult Function(Cart cart, List<Dish>? dishes)? success,
    TResult Function(String error, Cart cart, List<Dish>? dishes)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(cart, dishes);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitalState value) initial,
    required TResult Function(LoadingCartPageState value) loading,
    required TResult Function(SuccessCartPageState value) success,
    required TResult Function(ErrorCartPageState value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingCartPageState value)? loading,
    TResult Function(SuccessCartPageState value)? success,
    TResult Function(ErrorCartPageState value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingCartPageState value)? loading,
    TResult Function(SuccessCartPageState value)? success,
    TResult Function(ErrorCartPageState value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class LoadingCartPageState implements CartPageState {
  const factory LoadingCartPageState(
      {required final Cart cart,
      final List<Dish>? dishes}) = _$LoadingCartPageState;

  @override
  Cart get cart;
  @override
  List<Dish>? get dishes;
  @override
  @JsonKey(ignore: true)
  _$$LoadingCartPageStateCopyWith<_$LoadingCartPageState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SuccessCartPageStateCopyWith<$Res>
    implements $CartPageStateCopyWith<$Res> {
  factory _$$SuccessCartPageStateCopyWith(_$SuccessCartPageState value,
          $Res Function(_$SuccessCartPageState) then) =
      __$$SuccessCartPageStateCopyWithImpl<$Res>;
  @override
  $Res call({Cart cart, List<Dish>? dishes});

  @override
  $CartCopyWith<$Res> get cart;
}

/// @nodoc
class __$$SuccessCartPageStateCopyWithImpl<$Res>
    extends _$CartPageStateCopyWithImpl<$Res>
    implements _$$SuccessCartPageStateCopyWith<$Res> {
  __$$SuccessCartPageStateCopyWithImpl(_$SuccessCartPageState _value,
      $Res Function(_$SuccessCartPageState) _then)
      : super(_value, (v) => _then(v as _$SuccessCartPageState));

  @override
  _$SuccessCartPageState get _value => super._value as _$SuccessCartPageState;

  @override
  $Res call({
    Object? cart = freezed,
    Object? dishes = freezed,
  }) {
    return _then(_$SuccessCartPageState(
      cart: cart == freezed
          ? _value.cart
          : cart // ignore: cast_nullable_to_non_nullable
              as Cart,
      dishes: dishes == freezed
          ? _value.dishes
          : dishes // ignore: cast_nullable_to_non_nullable
              as List<Dish>?,
    ));
  }
}

/// @nodoc

class _$SuccessCartPageState implements SuccessCartPageState {
  const _$SuccessCartPageState({required this.cart, required this.dishes});

  @override
  final Cart cart;
  @override
  final List<Dish>? dishes;

  @override
  String toString() {
    return 'CartPageState.success(cart: $cart, dishes: $dishes)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SuccessCartPageState &&
            const DeepCollectionEquality().equals(other.cart, cart) &&
            const DeepCollectionEquality().equals(other.dishes, dishes));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(cart),
      const DeepCollectionEquality().hash(dishes));

  @JsonKey(ignore: true)
  @override
  _$$SuccessCartPageStateCopyWith<_$SuccessCartPageState> get copyWith =>
      __$$SuccessCartPageStateCopyWithImpl<_$SuccessCartPageState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Cart cart, List<Dish>? dishes) initial,
    required TResult Function(Cart cart, List<Dish>? dishes) loading,
    required TResult Function(Cart cart, List<Dish>? dishes) success,
    required TResult Function(String error, Cart cart, List<Dish>? dishes)
        error,
  }) {
    return success(cart, dishes);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Cart cart, List<Dish>? dishes)? initial,
    TResult Function(Cart cart, List<Dish>? dishes)? loading,
    TResult Function(Cart cart, List<Dish>? dishes)? success,
    TResult Function(String error, Cart cart, List<Dish>? dishes)? error,
  }) {
    return success?.call(cart, dishes);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Cart cart, List<Dish>? dishes)? initial,
    TResult Function(Cart cart, List<Dish>? dishes)? loading,
    TResult Function(Cart cart, List<Dish>? dishes)? success,
    TResult Function(String error, Cart cart, List<Dish>? dishes)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(cart, dishes);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitalState value) initial,
    required TResult Function(LoadingCartPageState value) loading,
    required TResult Function(SuccessCartPageState value) success,
    required TResult Function(ErrorCartPageState value) error,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingCartPageState value)? loading,
    TResult Function(SuccessCartPageState value)? success,
    TResult Function(ErrorCartPageState value)? error,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingCartPageState value)? loading,
    TResult Function(SuccessCartPageState value)? success,
    TResult Function(ErrorCartPageState value)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class SuccessCartPageState implements CartPageState {
  const factory SuccessCartPageState(
      {required final Cart cart,
      required final List<Dish>? dishes}) = _$SuccessCartPageState;

  @override
  Cart get cart;
  @override
  List<Dish>? get dishes;
  @override
  @JsonKey(ignore: true)
  _$$SuccessCartPageStateCopyWith<_$SuccessCartPageState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ErrorCartPageStateCopyWith<$Res>
    implements $CartPageStateCopyWith<$Res> {
  factory _$$ErrorCartPageStateCopyWith(_$ErrorCartPageState value,
          $Res Function(_$ErrorCartPageState) then) =
      __$$ErrorCartPageStateCopyWithImpl<$Res>;
  @override
  $Res call({String error, Cart cart, List<Dish>? dishes});

  @override
  $CartCopyWith<$Res> get cart;
}

/// @nodoc
class __$$ErrorCartPageStateCopyWithImpl<$Res>
    extends _$CartPageStateCopyWithImpl<$Res>
    implements _$$ErrorCartPageStateCopyWith<$Res> {
  __$$ErrorCartPageStateCopyWithImpl(
      _$ErrorCartPageState _value, $Res Function(_$ErrorCartPageState) _then)
      : super(_value, (v) => _then(v as _$ErrorCartPageState));

  @override
  _$ErrorCartPageState get _value => super._value as _$ErrorCartPageState;

  @override
  $Res call({
    Object? error = freezed,
    Object? cart = freezed,
    Object? dishes = freezed,
  }) {
    return _then(_$ErrorCartPageState(
      error: error == freezed
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
      cart: cart == freezed
          ? _value.cart
          : cart // ignore: cast_nullable_to_non_nullable
              as Cart,
      dishes: dishes == freezed
          ? _value.dishes
          : dishes // ignore: cast_nullable_to_non_nullable
              as List<Dish>?,
    ));
  }
}

/// @nodoc

class _$ErrorCartPageState implements ErrorCartPageState {
  const _$ErrorCartPageState(
      {required this.error, required this.cart, this.dishes});

  @override
  final String error;
  @override
  final Cart cart;
  @override
  final List<Dish>? dishes;

  @override
  String toString() {
    return 'CartPageState.error(error: $error, cart: $cart, dishes: $dishes)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ErrorCartPageState &&
            const DeepCollectionEquality().equals(other.error, error) &&
            const DeepCollectionEquality().equals(other.cart, cart) &&
            const DeepCollectionEquality().equals(other.dishes, dishes));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(error),
      const DeepCollectionEquality().hash(cart),
      const DeepCollectionEquality().hash(dishes));

  @JsonKey(ignore: true)
  @override
  _$$ErrorCartPageStateCopyWith<_$ErrorCartPageState> get copyWith =>
      __$$ErrorCartPageStateCopyWithImpl<_$ErrorCartPageState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Cart cart, List<Dish>? dishes) initial,
    required TResult Function(Cart cart, List<Dish>? dishes) loading,
    required TResult Function(Cart cart, List<Dish>? dishes) success,
    required TResult Function(String error, Cart cart, List<Dish>? dishes)
        error,
  }) {
    return error(this.error, cart, dishes);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Cart cart, List<Dish>? dishes)? initial,
    TResult Function(Cart cart, List<Dish>? dishes)? loading,
    TResult Function(Cart cart, List<Dish>? dishes)? success,
    TResult Function(String error, Cart cart, List<Dish>? dishes)? error,
  }) {
    return error?.call(this.error, cart, dishes);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Cart cart, List<Dish>? dishes)? initial,
    TResult Function(Cart cart, List<Dish>? dishes)? loading,
    TResult Function(Cart cart, List<Dish>? dishes)? success,
    TResult Function(String error, Cart cart, List<Dish>? dishes)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this.error, cart, dishes);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InitalState value) initial,
    required TResult Function(LoadingCartPageState value) loading,
    required TResult Function(SuccessCartPageState value) success,
    required TResult Function(ErrorCartPageState value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingCartPageState value)? loading,
    TResult Function(SuccessCartPageState value)? success,
    TResult Function(ErrorCartPageState value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InitalState value)? initial,
    TResult Function(LoadingCartPageState value)? loading,
    TResult Function(SuccessCartPageState value)? success,
    TResult Function(ErrorCartPageState value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class ErrorCartPageState implements CartPageState {
  const factory ErrorCartPageState(
      {required final String error,
      required final Cart cart,
      final List<Dish>? dishes}) = _$ErrorCartPageState;

  String get error;
  @override
  Cart get cart;
  @override
  List<Dish>? get dishes;
  @override
  @JsonKey(ignore: true)
  _$$ErrorCartPageStateCopyWith<_$ErrorCartPageState> get copyWith =>
      throw _privateConstructorUsedError;
}

part of 'cart_page_provider.dart';

@Freezed(makeCollectionsUnmodifiable: false)
class CartPageState with _$CartPageState {
  const factory CartPageState.initial({
    required Cart cart,
    List<Dish>? dishes,
  }) = _InitalState;

  const factory CartPageState.loading({
    required Cart cart,
    List<Dish>? dishes,
  }) = LoadingCartPageState;

  const factory CartPageState.success({
    required Cart cart,
    required List<Dish>? dishes,
  }) = SuccessCartPageState;

  const factory CartPageState.error({
    required String error,
    required Cart cart,
    List<Dish>? dishes,
  }) = ErrorCartPageState;
}

import 'package:flutter/material.dart';
import 'package:marketfood_test_app/features/cart_page/data/models/cart/cart.dart';
import 'package:marketfood_test_app/features/cart_page/presentation/widgets/cart_list_item.dart';

class CartList extends StatelessWidget {
  final Cart cart;

  const CartList({
    Key? key,
    required this.cart,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: cart.cartDishes.length,
      itemBuilder: (BuildContext context, int index) {
        return CartListItem(cartDishItem: cart.cartDishes[index]);
      },
    );
  }
}

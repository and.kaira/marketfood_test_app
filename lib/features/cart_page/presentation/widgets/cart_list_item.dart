import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:marketfood_test_app/features/cart_page/data/models/cart_dish_item/cart_dish_item.dart';
import 'package:marketfood_test_app/features/cart_page/presentation/provider/cart_page_provider.dart';
import 'package:marketfood_test_app/src/app_data/app_colors.dart';
import 'package:marketfood_test_app/src/app_data/app_fonts.dart';

class CartListItem extends HookConsumerWidget {
  final CartDishItem cartDishItem;

  const CartListItem({
    Key? key,
    required this.cartDishItem,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final provider = ref.read<CartPageProvider>(cartPageProvider.notifier);
    return Container(
      decoration: BoxDecoration(
        border: Border.all(),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            Text(
              cartDishItem.dish.name,
              style: AppFonts.robotoBold.copyWith(fontSize: 20),
            ),
            const Spacer(),
            Row(
              children: [
                IconButton(
                  onPressed: () => provider.decrementCountCartDishItem(cartDishItem),
                  icon: Icon(Icons.remove, color: Colors.red, size: 30),
                ),
                Text(
                  cartDishItem.count.toString(),
                  style: AppFonts.robotoBold.copyWith(fontSize: 28),
                ),
                IconButton(
                  onPressed: () => provider.incrementCountCartDishItem(cartDishItem),
                  icon: Icon(Icons.add, color: Colors.green, size: 30),
                ),
              ],
            ),
            IconButton(
              onPressed: () => provider.removeDishToCart(cartDishItem),
              icon: Icon(Icons.delete, color: AppColors.red, size: 30),
            ),
          ],
        ),
      ),
    );
  }
}

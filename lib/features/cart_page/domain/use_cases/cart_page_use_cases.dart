import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:marketfood_test_app/features/cart_page/data/data_sources/i_cart_page_data_sources.dart';
import 'package:marketfood_test_app/features/cart_page/data/models/dish/dish.dart';

class CartPageUseCase implements ICartPageDataSource {
  static const String _dishCollectionName = 'dishes';

  @override
  Future<List<Dish>?> getDishes() async {
    final data = await FirebaseFirestore.instance.collection(_dishCollectionName).get();

    List<Dish> dishes = [];
    data.docs.forEach((element) {
      dishes.add(Dish.fromJson(element.data()));
    });
    return dishes;
  }
}

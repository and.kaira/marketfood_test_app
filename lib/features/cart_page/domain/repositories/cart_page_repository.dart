import 'package:marketfood_test_app/features/cart_page/data/data_sources/i_cart_page_data_sources.dart';
import 'package:marketfood_test_app/features/cart_page/data/models/dish/dish.dart';

class CartPageRepository {
  final ICartPageDataSource _cartPageDataSource;

  CartPageRepository(this._cartPageDataSource);

  Future<List<Dish>?> getDishes() {
    return _cartPageDataSource.getDishes();
  }
}

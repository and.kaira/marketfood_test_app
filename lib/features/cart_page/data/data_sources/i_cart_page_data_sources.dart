import 'package:marketfood_test_app/features/cart_page/data/models/dish/dish.dart';

abstract class ICartPageDataSource {
  Future<List<Dish>?> getDishes();
}

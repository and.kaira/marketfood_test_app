// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cart_dish_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CartDishItem _$$_CartDishItemFromJson(Map<String, dynamic> json) =>
    _$_CartDishItem(
      dish: Dish.fromJson(json['dish'] as Map<String, dynamic>),
      count: json['count'] as int? ?? 1,
    );

Map<String, dynamic> _$$_CartDishItemToJson(_$_CartDishItem instance) =>
    <String, dynamic>{
      'dish': instance.dish,
      'count': instance.count,
    };

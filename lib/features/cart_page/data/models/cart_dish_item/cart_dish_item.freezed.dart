// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'cart_dish_item.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CartDishItem _$CartDishItemFromJson(Map<String, dynamic> json) {
  return _CartDishItem.fromJson(json);
}

/// @nodoc
mixin _$CartDishItem {
  Dish get dish => throw _privateConstructorUsedError;
  set dish(Dish value) => throw _privateConstructorUsedError;
  int get count => throw _privateConstructorUsedError;
  set count(int value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CartDishItemCopyWith<CartDishItem> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CartDishItemCopyWith<$Res> {
  factory $CartDishItemCopyWith(
          CartDishItem value, $Res Function(CartDishItem) then) =
      _$CartDishItemCopyWithImpl<$Res>;
  $Res call({Dish dish, int count});

  $DishCopyWith<$Res> get dish;
}

/// @nodoc
class _$CartDishItemCopyWithImpl<$Res> implements $CartDishItemCopyWith<$Res> {
  _$CartDishItemCopyWithImpl(this._value, this._then);

  final CartDishItem _value;
  // ignore: unused_field
  final $Res Function(CartDishItem) _then;

  @override
  $Res call({
    Object? dish = freezed,
    Object? count = freezed,
  }) {
    return _then(_value.copyWith(
      dish: dish == freezed
          ? _value.dish
          : dish // ignore: cast_nullable_to_non_nullable
              as Dish,
      count: count == freezed
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }

  @override
  $DishCopyWith<$Res> get dish {
    return $DishCopyWith<$Res>(_value.dish, (value) {
      return _then(_value.copyWith(dish: value));
    });
  }
}

/// @nodoc
abstract class _$$_CartDishItemCopyWith<$Res>
    implements $CartDishItemCopyWith<$Res> {
  factory _$$_CartDishItemCopyWith(
          _$_CartDishItem value, $Res Function(_$_CartDishItem) then) =
      __$$_CartDishItemCopyWithImpl<$Res>;
  @override
  $Res call({Dish dish, int count});

  @override
  $DishCopyWith<$Res> get dish;
}

/// @nodoc
class __$$_CartDishItemCopyWithImpl<$Res>
    extends _$CartDishItemCopyWithImpl<$Res>
    implements _$$_CartDishItemCopyWith<$Res> {
  __$$_CartDishItemCopyWithImpl(
      _$_CartDishItem _value, $Res Function(_$_CartDishItem) _then)
      : super(_value, (v) => _then(v as _$_CartDishItem));

  @override
  _$_CartDishItem get _value => super._value as _$_CartDishItem;

  @override
  $Res call({
    Object? dish = freezed,
    Object? count = freezed,
  }) {
    return _then(_$_CartDishItem(
      dish: dish == freezed
          ? _value.dish
          : dish // ignore: cast_nullable_to_non_nullable
              as Dish,
      count: count == freezed
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CartDishItem implements _CartDishItem {
  _$_CartDishItem({required this.dish, this.count = 1});

  factory _$_CartDishItem.fromJson(Map<String, dynamic> json) =>
      _$$_CartDishItemFromJson(json);

  @override
  Dish dish;
  @override
  @JsonKey()
  int count;

  @override
  String toString() {
    return 'CartDishItem(dish: $dish, count: $count)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_CartDishItemCopyWith<_$_CartDishItem> get copyWith =>
      __$$_CartDishItemCopyWithImpl<_$_CartDishItem>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CartDishItemToJson(
      this,
    );
  }
}

abstract class _CartDishItem implements CartDishItem {
  factory _CartDishItem({required Dish dish, int count}) = _$_CartDishItem;

  factory _CartDishItem.fromJson(Map<String, dynamic> json) =
      _$_CartDishItem.fromJson;

  @override
  Dish get dish;
  set dish(Dish value);
  @override
  int get count;
  set count(int value);
  @override
  @JsonKey(ignore: true)
  _$$_CartDishItemCopyWith<_$_CartDishItem> get copyWith =>
      throw _privateConstructorUsedError;
}

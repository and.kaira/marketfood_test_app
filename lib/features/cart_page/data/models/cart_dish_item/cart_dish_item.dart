import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:marketfood_test_app/features/cart_page/data/models/dish/dish.dart';

part 'cart_dish_item.freezed.dart';
part 'cart_dish_item.g.dart';

@unfreezed
class CartDishItem with _$CartDishItem {
  factory CartDishItem({
    required Dish dish,
    @Default(1) int count,
  }) = _CartDishItem;

  factory CartDishItem.fromJson(Map<String, dynamic> json) => _$CartDishItemFromJson(json);
}

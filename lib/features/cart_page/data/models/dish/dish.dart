import 'package:freezed_annotation/freezed_annotation.dart';

part 'dish.freezed.dart';
part 'dish.g.dart';

@Freezed(toJson: true, fromJson: true)
class Dish with _$Dish  {
  const factory Dish({
    required String id,
    required String image,
    required String name,
    required String description,
    required double weight,
    required String weightUnit,
  }) = _Dish;

  factory Dish.fromJson(Map<String, dynamic> json) =>
      _$DishFromJson(json);
}

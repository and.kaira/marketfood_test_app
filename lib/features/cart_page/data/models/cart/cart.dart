import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:marketfood_test_app/features/cart_page/data/models/cart_dish_item/cart_dish_item.dart';

part 'cart.freezed.dart';
part 'cart.g.dart';

@Freezed(toJson: true, fromJson: true, makeCollectionsUnmodifiable: false)
class Cart with _$Cart  {
  const factory Cart({
    required List<CartDishItem> cartDishes,
  }) = _Cart;

  factory Cart.fromJson(Map<String, dynamic> json) =>
      _$CartFromJson(json);
}

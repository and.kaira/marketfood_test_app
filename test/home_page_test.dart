import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:marketfood_test_app/features/home_page/presentation/pages/home_page.dart';
import 'package:marketfood_test_app/src/application/app_router/app_scaffold.dart';

final counterProvider = StateProvider((ref) => 0);

// Renders the current state and a button that allows incrementing the state
class TestWidget extends StatelessWidget {
  const TestWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const Material(
      child: AppScaffold(
        location: '/',
        child: HomePage(),
      ),
    );
  }
}

void main() {
  testWidgets('Test home page', (tester) async {
    await tester.pumpWidget(const ProviderScope(child: TestWidget()));


  });
}
